const settings = {
  "name": "stream-hub",
  "state": {
    "frontity": {
      url: "https://stream-hub.com",
      title: "Stream Hub",
      description: "Trang web về thủ thuật điện thoại, máy tính, học lập trình, sửa lỗi máy tính, cách dùng các phần mềm, phần mềm chuyên dụng, công nghệ khoa học"
    }
  },
  "packages": [
    {
      "name": "@frontity/twentytwenty-theme",
      "state": {
        theme: {
          color: "#0073aa",
          menu: [
            ["Giới thiệu", "/gioi-thieu/"],
            ["Điện thoại", "/category/dien-thoai/"],
            ["Máy tính", "/category/may-tinh/"],
            ["Lắp máy", "/category/phan-cung/"],
            ["Mua Backlink", "/backlink-chat-luong/"],
          ],socialLinks
          : [
            ["pinterest", "https://www.pinterest.com/streamhubvn/"],
            ["facebook", "https://www.facebook.com/streamhubvn/"],
            ["twitter", "https://twitter.com/streamhubvn/"]
          ],
          featured: {
            showOnList: false,
            showOnPost: true
          }
        }
      }
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "api": "https://wp.stream-hub.com/wp-json"
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react",
    "@frontity/head-tags"
  ]
};

export default settings;
