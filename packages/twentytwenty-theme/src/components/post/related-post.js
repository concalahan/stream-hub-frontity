import { styled, connect } from "frontity";
import React from "react";
import Link from "../link";

const RelatedWrap = styled.div`
    display:flex;
    @media (max-width: 48em){
        display: block;
    }
`;

const RelatedBox = styled.div`
    position: relative;
    width: 31%;
    margin: 0 calc(7%/6);
    text-align: center;
    padding: 40px 1px;
    background: linear-gradient(to right,#89216B,#DA4453);
    border-radius: 6px;
    @media (max-width: 48em){
        width: 100%;
        margin: 20px 0;
    }
    a{
        color: #fff;
        text-decoration: none;
        padding: 0 10px;
    }
    .flow-btn{
        position: absolute;
        font-size: 28px;
        right: 4%;
        bottom: 4%;
        color: #fff;
    }
`;


const Related = ({ state, item }) => {
    const isSimilarTo = item.acf && item.acf.isSimilarTo ? item.acf.isSimilarTo : null;

    if (!isSimilarTo) {
        return (
            <div></div>
        );
    }

    const postOne = isSimilarTo.issimilarto_1 ? isSimilarTo.issimilarto_1 : null;
    const postTwo = isSimilarTo.issimilarto_2 ? isSimilarTo.issimilarto_2 : null;
    const postThree = isSimilarTo.issimilarto_3 ? isSimilarTo.issimilarto_3 : null;

    const posts = [postOne, postTwo, postThree];

    return (
        <RelatedWrap>
            {posts.map((post, index) => {
                if (post) {
                    return (
                        <RelatedBox key={index}>
                            <a href={`${state.frontity.url}/${post.post_name}/`}>
                                {post.post_title}
                                <div className="flow-btn">→</div>
                            </a>
                        </RelatedBox>
                    )
                }
            })}

        </RelatedWrap>
    )
}

export default connect(Related);