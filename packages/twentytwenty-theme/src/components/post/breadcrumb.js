import { styled, connect } from "frontity";
import React from "react";
import Link from "../link";

const BreadcrumbWrap = styled.div`
    ul{
        display :flex;
        list-style-type: none;
        margin-top: 20px;
        margin-bottom: 0;
        li{
            margin: 0;
                &.have-link{
                    div {
                    &:after{
                        content: ">";
                        margin: 0 5px;
                        text-decoration: none;
                    }
                }
            }
        }
    }
    @media (max-width: 48em){
        ul{
            display: inline-block;
        }
    }
`;


const Breadcrumb = ({ state, categories, item }) => {
    const lastItem = item.title.rendered;

    // Remove empty or undefined categories
    let postCategories = categories ? categories.filter(Boolean) : [];

    var count = 1;

    if (postCategories.length === 0) {
        return null;
    }

    postCategories = postCategories.sort(function (a, b) {
        // ASC  -> a.length - b.length
        // DESC -> b.length - a.length
        return b.link.length - a.link.length;
    });

    return (
        <BreadcrumbWrap>
            <ul aria-label="breadcrumb" itemScope itemType="https://schema.org/BreadcrumbList">
                {postCategories.reverse().map((category, index) => (
                    <li key={index + 1} itemProp="itemListElement" itemScope itemType="https://schema.org/ListItem" className="have-link">
                        <div itemProp="item" content={`${state.frontity.url}${category.link}`}>
                            <StyledLink itemProp="item" link={category.link}>
                                <span itemProp="name">{category.name}</span>
                                <meta itemProp="url" content={`${state.frontity.url}${category.link}`} />
                            </StyledLink>
                        </div>

                        <meta itemProp="position" content={count++} />
                    </li>
                ))}

                <li key={4} itemProp="itemListElement" itemScope itemType="https://schema.org/ListItem">
                    <div itemProp="item" content={`${state.frontity.url}${item.link}`}>
                        <StyledLink itemProp="item" link={item.link}>
                            <span itemProp="name">{lastItem}</span>
                            <meta itemProp="url" content={`${state.frontity.url}${item.link}`} />
                        </StyledLink>
                    </div>
                    <meta itemProp="position" content={count} />
                </li>
            </ul>
        </BreadcrumbWrap>
    )
}

const StyledLink = styled(Link)`
    margin-right: 1rem;
`;

export default connect(Breadcrumb);
