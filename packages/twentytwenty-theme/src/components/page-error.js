import React from "react";
import { styled, connect } from "frontity";
import SearchForm from "./search/search-form";
import SectionContainer from "./styles/section-container";

const description404 = (
  <>
    Trang bạn đang tìm kiếm không có!! Tìm kiếm lại:
  </>
);

const description = (
  <>
    Đừng sợ, đã có chúng tôi ở đây, bạn thử f5 lại nhé. Hoặc bạn có thể tìm keyword:
  </>
);

// The Error page component
const ErrorPage = ({ state }) => {
  const data = state.source.get(state.router.link);

  const title = "Oops, có gì đó xảy ra";
  const title404 = "Oops! 404";

  return (
    <Container size="thin">
      {
        data ?
          <React.Fragment>
            <EntryTitle>{data.is404 ? title404 : title}</EntryTitle>
            <IntroText>{data.is404 ? description404 : description}</IntroText>
          </React.Fragment>
          : null
      }

      <SearchForm />
    </Container>
  );
};

export default connect(ErrorPage);

export const EntryTitle = styled.h1`
  margin: 0;

  @media (min-width: 700px) {
    font-size: 6.4rem !important;
  }

  @media (min-width: 1200px) {
    font-size: 8.4rem !important;
  }
`;

const IntroText = styled.div`
  margin-top: 2rem;
  line-height: 1.5;

  @media (min-width: 700px) {
    font-size: 2rem;
    margin-top: 2.5rem;
  }
`;

const Container = styled(SectionContainer)`
  text-align: center;
  padding-top: 8rem;
`;
